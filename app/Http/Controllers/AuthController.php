<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('/register');
    }

    public function welcome2(request $request){
        //dd($request->all());
        $fname = $request['fname'];
        $lname = $request['lname'];

        return view('welcome2', compact('fname','lname'));
    }
}
